﻿using System;

namespace ContactsApp.Core.Models
{
    public enum Gender
    {
        Man,
        Woman,
        DontKnow
    }

    public class Contact
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public DateTime BirthDay { get; set; }
        public int Age => DateTime.Today.Year - BirthDay.Year;
        public Gender Gender { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string FullAddress => $"{Street}, {PostalCode} {City}";
    }
}
