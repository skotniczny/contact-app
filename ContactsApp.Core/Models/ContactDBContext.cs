﻿
using Microsoft.EntityFrameworkCore;

namespace ContactsApp.Core.Models
{
    public class ContactDBContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlite("Data Source=contacts.db");
    }
}
