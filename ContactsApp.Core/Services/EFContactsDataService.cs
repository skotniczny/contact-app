﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactsApp.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace ContactsApp.Core.Services
{
    public class EFContactsDataService : IDataService
    {
        public async Task<IEnumerable<Contact>> AllContacts()
        {
            using (var db = new ContactDBContext())
            {
                return await db.Contacts
                    .OrderBy(item => item.LastName)
                    .ThenBy(item => item.FirstName)
                    .ToListAsync();
            }
        }

        public async Task<bool> SaveContact(Contact contact)
        {
            using (var db = new ContactDBContext())
            {
                if (contact.Id == 0)
                {
                    db.Contacts.Add(contact);
                    return await db.SaveChangesAsync() > 0;
                }
                else
                {
                    Contact dbEntry = db.Contacts
                        .FirstOrDefault(c => c.Id == contact.Id);
                    if (dbEntry != null)
                    {
                        dbEntry.FirstName = contact.FirstName;
                        dbEntry.LastName = contact.LastName;
                        dbEntry.Gender = contact.Gender;
                        dbEntry.BirthDay = contact.BirthDay;
                        dbEntry.PostalCode = contact.PostalCode;
                        dbEntry.City = contact.City;
                        dbEntry.Street = contact.Street;
                    }

                    return await db.SaveChangesAsync() > 0;
                }
            }
        }

        public async Task<Contact> GetContact(int id)
        {
            using (var db = new ContactDBContext())
            {
                return await db.Contacts
                    .FirstOrDefaultAsync(c => c.Id == id);
            }
        }

        public async Task<bool> RemoveContact(Contact contact)
        {
            if (contact == null) return false;
            using (var db = new ContactDBContext())
            {
                db.Remove(contact);
                return await db.SaveChangesAsync() > 0;
            }
        }
    }
}
