﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ContactsApp.Core.Models;

namespace ContactsApp.Core.Services
{
    public interface IDataService
    {
        Task<IEnumerable<Contact>> AllContacts();
        Task<Contact> GetContact(int id);
        Task<bool> RemoveContact(Contact contact);
        Task<bool> SaveContact(Contact contact);
    }
}