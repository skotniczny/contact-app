﻿using ContactsApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContactsApp.Core.Services
{
    // Sztuczny serwis użyty w trakcie tworzenia aplikacji
    // implementuje interfejs IDataService
    public class MemoryContactsDataService : IDataService
    {
        private static List<Contact> contacts = new List<Contact> {
                new Contact()
                {
                    Id = 1,
                    FirstName = "Jan",
                    LastName = "Kowalski",
                    BirthDay = new DateTime(2006, 10, 10),
                    PostalCode = "22-222",
                    Street = "Bułgarska",
                    City = "Swarzędz",
                    Gender = Gender.Man,
                },
                new Contact()
                {
                    Id = 2,
                    FirstName = "Adam",
                    LastName = "Nowak",
                    BirthDay = new DateTime(1997, 10, 1),
                    PostalCode = "22-222",
                    Street = "św. Marcin",
                    City = "Swarzędz",
                    Gender = Gender.Man,
        },
                new Contact()
                {
                    Id = 3,
                    FirstName = "Joanna",
                    LastName = "Wiśniewska",
                    BirthDay = new DateTime(1984, 10, 2),
                    PostalCode = "22-222",
                    Street = "Dworcowa",
                    City = "Swarzędz",
                    Gender = Gender.Woman,
                },
        };

        public async Task<IEnumerable<Contact>> AllContacts()
        {
            await Task.CompletedTask;
            return contacts;
        }

        public async Task<bool> SaveContact(Contact contact)
        {
            await Task.CompletedTask;
            if (contact.Id == 0)
            {
                contact.Id = contacts.Count + 1;
                contacts.Add(contact);
            }
            else
            {
                 contacts[contacts.FindIndex(item => item.Id == contact.Id)] = contact;
            }
            return true;
        }

        public async Task<Contact> GetContact(int id)
        {
            await Task.CompletedTask;
            return contacts.Find(contact => contact.Id == id);
        }

        public async Task<bool> RemoveContact(Contact contact)
        {
            await Task.CompletedTask;
            return contacts.Remove(contact);
        }
    }
}
