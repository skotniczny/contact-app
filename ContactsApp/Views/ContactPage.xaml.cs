﻿using ContactsApp.Core.Models;
using ContactsApp.Core.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace ContactsApp.Views
{
    /// <summary>
    /// Strona dodawania i edycji kontaktu
    /// </summary>
    public sealed partial class ContactPage : Page
    {
        private bool _isEditMode;
        private Contact _contact;
        //private readonly IDataService _dataService = new MemoryContactsDataService();
        private readonly IDataService _dataService = new EFContactsDataService();

        public ContactPage()
        {
            InitializeComponent();
            var genderEnumValue = Enum.GetValues(typeof(Gender)).Cast<Gender>();
            ContactGender.ItemsSource = genderEnumValue.ToList();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                _isEditMode = true;
                int contactId = (int) e.Parameter;
                _contact = await _dataService.GetContact(contactId);
                FillForm();
            }
            else
            {
                _isEditMode = false;
            }
        }

        private void FillForm()
        {
            ContactGender.SelectedItem = _contact.Gender;
            ContactFirstName.Text = _contact.FirstName;
            ContactLastName.Text = _contact.LastName;
            ContactPostalCode.Text = _contact.PostalCode;
            ContactCity.Text = _contact.City;
            ContactStreet.Text = _contact.Street;
            ContactBirthday.Date = _contact.BirthDay;
        }

        private async Task<int> AddOrUpdateContact()
        {
            if (!_isEditMode) _contact = new Contact();
            _contact.Gender = ParseGenderSelect();
            _contact.FirstName = ContactFirstName.Text;
            _contact.LastName = ContactLastName.Text;
            _contact.PostalCode = ContactPostalCode.Text;
            _contact.City = ContactCity.Text;
            _contact.Street = ContactStreet.Text;
            _contact.BirthDay = ContactBirthday.Date.UtcDateTime;
            await _dataService.SaveContact(_contact);
            return _contact.Id;
        }

        private Gender ParseGenderSelect()
        {
            if (!Enum.TryParse(ContactGender.SelectedItem?.ToString() ?? "", true, out Gender genderValue))
                genderValue = Gender.DontKnow;
            return genderValue;
        }

        private async void Submit_OnClick(object sender, RoutedEventArgs e)
        {
            int id = await AddOrUpdateContact();
            Frame.Navigate(typeof(MasterDetailPage), id);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack) Frame.GoBack();
        }
    }
}
