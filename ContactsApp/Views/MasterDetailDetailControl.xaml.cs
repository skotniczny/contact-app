﻿using System;

using ContactsApp.Core.Models;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace ContactsApp.Views
{
    public sealed partial class MasterDetailDetailControl : UserControl
    {
        public Contact MasterMenuItem
        {
            get { return GetValue(MasterMenuItemProperty) as Contact; }
            set { SetValue(MasterMenuItemProperty, value); }
        }

        public static readonly DependencyProperty MasterMenuItemProperty = DependencyProperty.Register("MasterMenuItem", typeof(Contact), typeof(MasterDetailDetailControl), new PropertyMetadata(null, OnMasterMenuItemPropertyChanged));

        public MasterDetailDetailControl()
        {
            InitializeComponent();
        }

        private static void OnMasterMenuItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as MasterDetailDetailControl;
            control.ForegroundElement.ChangeView(0, 0, 1);
        }
    }
}
