﻿using ContactsApp.Core.Helpers;
using ContactsApp.Core.Models;
using ContactsApp.Core.Services;
using ContactsApp.Helpers;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace ContactsApp.Views
{
    public sealed partial class MasterDetailPage : Page, INotifyPropertyChanged
    {
        //private readonly IDataService _dataService = new MemoryContactsDataService();
        private readonly IDataService _dataService = new EFContactsDataService();
        private Contact _selected;
        private int _contactIdParameter;

        public MasterDetailPage()
        {
            InitializeComponent();
            Loaded += MasterDetailPage_Loaded;
        }

        public Contact Selected
        {
            get => _selected;
            set => Set(ref _selected, value);
        }

        public ObservableCollection<Contact> ContactItems { get; } = new ObservableCollection<Contact>();

        public event PropertyChangedEventHandler PropertyChanged;

        private void AddContact_OnClick(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ContactPage));
        }

        private void AutoSuggestBox_SuggestionChosen(AutoSuggestBox sender,
            AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            Selected = ContactItems.FirstOrDefault(item => item.FullName == args.SelectedItem.ToString());
        }

        private void AutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            var suitableItems = new List<string>();
            var splitText = sender.Text.ToLower().Split(" ");
            foreach (var contact in ContactItems)
            {
                var found = splitText.All(key => contact.FullName.ToLower().Contains(key) || contact.FullAddress.ToLower().Contains(key));
                if (found) suitableItems.Add(contact.FullName);
            }

            if (suitableItems.Count == 0) suitableItems.Add("Search_NotFound".GetLocalized());
            sender.ItemsSource = suitableItems;
        }

        private async void EditContact_OnClick(object sender, RoutedEventArgs e)
        {
            if (Selected == null)
            {
                ContentDialog dialog = new ContentDialog
                {
                    Title = "Dialog_ErrorTitle".GetLocalized(),
                    Content = "Dialog_EditContactError".GetLocalized(),
                    CloseButtonText = "Dialog_CloseButtonText".GetLocalized()
                };
                await dialog.ShowAsync();
            }
            else
            {
                Frame.Navigate(typeof(ContactPage), Selected.Id);
            }
        }

        private async void MasterDetailPage_Loaded(object sender, RoutedEventArgs e)
        {
            ContactItems.Clear();

            var data = await _dataService.AllContacts();

            foreach (var item in data) ContactItems.Add(item);

            if (MasterDetailsViewControl.ViewState == ListDetailsViewState.Both)
                Selected = ContactItems.FirstOrDefault(item => item.Id == _contactIdParameter);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (int.TryParse(e.Parameter.ToString(), out var result))
                _contactIdParameter = (int) e.Parameter;
            else
                _contactIdParameter = 0;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void RemoveContact_OnClick(object sender, RoutedEventArgs e)
        {
            bool isRemoved = await _dataService.RemoveContact(Selected);
            if (isRemoved)
            {
                ContactItems.Remove(Selected);
                Selected = ContactItems.FirstOrDefault();
            }
            else
            {
                ContentDialog dialog = new ContentDialog
                {
                    Title = "Dialog_ErrorTitle".GetLocalized(),
                    Content = "Dialog_RemoveContactError".GetLocalized(),
                    CloseButtonText = "Dialog_CloseButtonText".GetLocalized()
                };
                await dialog.ShowAsync();
            }
        }

        private void Set<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Equals(storage, value)) return;

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void Settings_OnClick(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SettingsPage));
        }

        private async void Save_OnClick(object sender, RoutedEventArgs e)
        {
            string jsonData = await Json.StringifyAsync(ContactItems);

            var savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add("JSON", new List<string>() { ".json" });
            // Default file name if the user does not type one in or select a file to replace
            savePicker.SuggestedFileName = "Export_SuggestedFileName".GetLocalized() + "_" + DateTime.Today.ToString("yy-MM-dd");

            StorageFile file = await savePicker.PickSaveFileAsync();
            if (file != null)
            {
                // Prevent updates to the remote version of the file until
                // we finish making changes and call CompleteUpdatesAsync.
                CachedFileManager.DeferUpdates(file);
                // write to file
                await FileIO.WriteTextAsync(file, jsonData);
                // Let Windows know that we're finished changing the file so
                // the other app can update the remote version of the file.
                // Completing updates may require Windows to ask for user input.
                Windows.Storage.Provider.FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                if (status == Windows.Storage.Provider.FileUpdateStatus.Complete)
                {
                    ContentDialog dialog = new ContentDialog
                    {
                        Title = "Dialog_ExportTitle".GetLocalized(),
                        Content = "Dialog_ExportContent".GetLocalized() + file.Name,
                        CloseButtonText = "Dialog_CloseButtonText".GetLocalized()
                    };
                    await dialog.ShowAsync();
                }
                else
                {
                    ContentDialog dialog = new ContentDialog
                    {
                        Title = "Dialog_ExportTitle".GetLocalized(),
                        Content = "Didalog_ExportContentError".GetLocalized() + file.Name,
                        CloseButtonText = "Dialog_CloseButtonText".GetLocalized()
                    };
                    await dialog.ShowAsync();
                }
            }
        }
    }
}
